#ifndef CPPCLASS_H
#define CPPCLASS_H

#include <QObject>
#include <QtQml>
#include <QDebug>

class CppClass : public QObject{
    Q_OBJECT

public:
    CppClass(QObject *parent = 0) : QObject(parent) { }
    ~CppClass() { }
};

#endif // CPPCLASS_H
