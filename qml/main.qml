// main.qml

import QtQuick 2.0
import QtQuick.Controls 1.0
//import QtQuick.Layouts 1.0
//import QtQuick.Window 2.1

import CppClassModule 1.0

ApplicationWindow {
    id: app
    title: qsTr("Template")
    width: 400
    height: 300
    visible: true

    CppClass {
        id: cppClassInQml
    }
}

